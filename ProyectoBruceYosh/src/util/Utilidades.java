package util;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class Utilidades {
	public static void notificaError (JFrame padre, String titulo, Exception e, String mensaje) {
		String contenido="";
		if (mensaje!=null) 
			contenido+=mensaje;
		if (e!=null && mensaje!=null) 
			contenido+= "\n----------------------------------------------------\n";
		if (e!=null) 
			contenido+=e.getClass().getName()+"\n"+e.getMessage();
		JOptionPane.showMessageDialog(padre,contenido,titulo,JOptionPane.ERROR_MESSAGE);
	}
	
	public static boolean preguntaUsuario(JFrame padre, String titulo, String mensaje){
		///Mensaje de confirmacion. Devuelve true si el usuario pulsa SI
		return JOptionPane.showConfirmDialog(padre, mensaje,titulo,JOptionPane.YES_NO_OPTION)==JOptionPane.YES_OPTION;
	}
	
}
