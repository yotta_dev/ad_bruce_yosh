package ejercicio3;

import java.util.ArrayList;

public class Info_ImpExp {
	String nombreBD;
	String nombreTabla;
	ArrayList<Columna> columnas; // Alternative Set clave valor 
	
	public String getNombreBD() {
		return nombreBD;
	}
	public void setNombreBD(String nombreBD) {
		this.nombreBD = nombreBD;
	}
	public String getNombreTabla() {
		return nombreTabla;
	}
	public void setNombreTabla(String nombreTabla) {
		this.nombreTabla = nombreTabla;
	}
	public ArrayList<Columna> getColumnas() {
		return columnas;
	}
	public void setColumnas(ArrayList<Columna> columnas) {
		this.columnas = columnas;
	}
	
	public Info_ImpExp(String nombreBD, String nombreTabla, ArrayList<Columna> columnas) {
		super();
		this.nombreBD = nombreBD;
		this.nombreTabla = nombreTabla;
		this.columnas = columnas;
	}
	
	@Override
	public String toString() {
		return "Info_ImpExp [nombreBD=" + nombreBD + ", nombreTabla=" + nombreTabla + ", columnas=" + columnas + "]";
	}
	
}
