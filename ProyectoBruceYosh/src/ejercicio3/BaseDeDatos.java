package ejercicio3;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;

import java.sql.DatabaseMetaData;

public class BaseDeDatos {
	
	Connection cn;
	Statement st;
	DatabaseMetaData metaDatos;
	ResultSet rs;
	
	public void conectar(String ip, String usu, String pass) throws Exception {
		Class.forName("com.mysql.jdbc.Driver");
		cn = DriverManager.getConnection("jdbc:mysql://"+ip+"/",usu,pass);
		metaDatos = (DatabaseMetaData) cn.getMetaData();
	}
	
	public void desconectar() throws SQLException{
		cn.close();
		cn = null;
	}
	
	public Vector<String> listarBases() throws SQLException {
		Vector<String> nombresBD = new Vector<String>();
		rs = metaDatos.getCatalogs();
		while (rs.next()) {
			nombresBD.add(rs.getString(1));
		}
		return nombresBD;
	}
	
	public Vector<String> listarTablas(String base) throws SQLException {
		Vector<String> nombresTablas = new Vector<String>();
		rs = metaDatos.getTables(base,null,"%",null);
		while(rs.next()) {
			nombresTablas.add(rs.getString(3));
		}
		return nombresTablas;
	}
	
	public boolean existeTabla(String base, String tabla) throws SQLException{
		Vector<String> lt=listarTablas(base);
		for(String t:lt)
			if(t.toLowerCase().equals(tabla.toLowerCase()))
				return true;
		return false;
	}
	
	public boolean existeBD(String base) throws SQLException{
		Vector<String>lb = listarBases();
		for(String b:lb)
			if(b.toLowerCase().equals(base.toLowerCase()))
				return true;
		return false;
	}
	
	public void crearBD(String base) throws SQLException {
		Statement st = cn.createStatement();
		String sql = "CREATE DATABASE IF NOT EXISTS" + base;
		st.executeUpdate(sql);
	}
	
	public void creatTabla(Info_ImpExp obj) throws SQLException{
		Statement stmt = cn.createStatement();
		String sql = "CREATE TABLE "+obj.getNombreBD()+"."+obj.getNombreTabla()+" (";
		for(int i=0; i<obj.getColumnas().size();i++) {
			sql+=obj.getColumnas().get(i).getNombre()+" "+obj.getColumnas().get(i).getTipo();
			if(i<(obj.getColumnas().size()-1))
				sql+=",";
			else
				sql+=")";
					
		}
		stmt.executeUpdate(sql);
	}
	
	public void borrarTabla(String base, String tabla) throws SQLException{
		Statement stm = cn.createStatement();
		String sql = "DROP TABLE"+base+"."+tabla;
		stm.executeUpdate(sql);
	}
}
