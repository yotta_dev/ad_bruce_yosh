package ejercicio3;

import java.awt.EventQueue;
import java.awt.event.WindowEvent;
import java.io.File;
import java.sql.SQLException;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import util.Utilidades;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;
import javax.swing.JList;
import javax.swing.JScrollPane;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ImportarExportarXMLGUI extends JFrame {

	/**
	 * 
	 */
	private JPanel contentPane;
	static JFrame padre;
	private JFrame ventana = this;
	static BaseDeDatos bd = VentanaCargarConfiguracion.bd;
	static String baseAUsar = null;
	Vector<String>nombreBases = null;
	Vector<String>nombreTablas = null;
	private JTextField textField;
	private File ficheroXML;
	private JButton btnImportar;


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ImportarExportarXMLGUI frame = new ImportarExportarXMLGUI(padre);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	protected void processWindowEvent (WindowEvent e) {
		super.processWindowEvent(e);
		if (e.getID()==WindowEvent.WINDOW_CLOSING) {
			padre.setEnabled(true);
			padre.setDefaultCloseOperation(EXIT_ON_CLOSE);
		}
	}

	/**
	 * Create the frame.
	 */
	public ImportarExportarXMLGUI(JFrame ventanaPadre) {
		ImportarExportarXMLGUI.padre = ventanaPadre;
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 767, 539);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(0, 0, 761, 481);
		contentPane.add(tabbedPane);
		try {
			nombreBases = bd.listarBases();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(ventana, "Error al cargar las bases de Datos","Error",JOptionPane.ERROR_MESSAGE);
		}
		
		JPanel panelTabExportar = new JPanel();
		tabbedPane.addTab("Importar", null, panelTabExportar, null);
		panelTabExportar.setLayout(null);
		
		JLabel lblIntroduzcaRutaDel = new JLabel("Introduzca ruta del fichero a importar (.xml extension)");
		lblIntroduzcaRutaDel.setBounds(116, 165, 386, 20);
		panelTabExportar.add(lblIntroduzcaRutaDel);
		
		textField = new JTextField();
		textField.setBounds(116, 201, 386, 26);
		panelTabExportar.add(textField);
		textField.setColumns(10);
		
		JButton btnNavegar = new JButton();
		btnNavegar.setIcon(new ImageIcon("directory_image.jpg"));
		btnNavegar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ficheroXML = seleccionarFicheroXML();
			}
		});
		btnNavegar.setBounds(527, 200, 50, 30);
		panelTabExportar.add(btnNavegar);
		
		btnImportar = new JButton("IMPORTAR");
		btnImportar.setBounds(316, 243, 115, 29);
		btnImportar.setEnabled(false);
		panelTabExportar.add(btnImportar);
		
		JPanel panelTabImportar = new JPanel();
		tabbedPane.addTab("Exportar", null, panelTabImportar, null);
		panelTabImportar.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(22, 40, 217, 358);
		panelTabImportar.add(scrollPane);
		
		JList listBase = new JList();
		scrollPane.setViewportView(listBase);
		listBase.setListData(nombreBases);
		
		JLabel lblSeleccionarBase = new JLabel("Seleccione una Base de Datos: ");
		lblSeleccionarBase.setBounds(12, 13, 229, 15);
		panelTabImportar.add(lblSeleccionarBase);
		
		JLabel lblBaseDeDatos = new JLabel("Base de Datos Seleccionada: ");
		lblBaseDeDatos.setBounds(22, 410, 600, 32);
		lblBaseDeDatos.setVisible(false);
		panelTabImportar.add(lblBaseDeDatos);
		
		JLabel lblSeleccioneUnaTabla = new JLabel("Seleccione una tabla:");
		lblSeleccioneUnaTabla.setBounds(300, 10, 149, 20);
		lblSeleccioneUnaTabla.setVisible(false);
		panelTabImportar.add(lblSeleccioneUnaTabla);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(290, 40, 217, 358);
		panelTabImportar.add(scrollPane_1);
		
		JList listTablas = new JList();
		listTablas.setBounds(310, 50, 215, 0);
		scrollPane_1.setViewportView(listTablas);
		scrollPane_1.setEnabled(false);
		scrollPane_1.setVisible(false);
		listTablas.setEnabled(false);
		
		JButton btnExportarTabla = new JButton("Exportar tabla");
		btnExportarTabla.setBounds(554, 172, 149, 29);
		btnExportarTabla.setEnabled(false);
		btnExportarTabla.setVisible(false);
		panelTabImportar.add(btnExportarTabla);
		listTablas.setVisible(false);
		listBase.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				baseAUsar = nombreBases.get(listBase.getSelectedIndex());
				lblBaseDeDatos.setText("Base de Datos Seleccionada: "+baseAUsar);
				lblBaseDeDatos.setVisible(true);
				try {
					
					nombreTablas = bd.listarTablas(nombreBases.get(listBase.getSelectedIndex()));
					scrollPane_1.setEnabled(true);
					listTablas.setEnabled(true);
					scrollPane_1.setVisible(true);
					listTablas.setVisible(true);
					lblSeleccioneUnaTabla.setVisible(true);
					btnExportarTabla.setEnabled(false);
					btnExportarTabla.setVisible(false);
					
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					Utilidades.notificaError(ventana, "Error listado", e, "Error al listar"
							+" la tabla de la base de datos seleccionada. Mensaje tecnico: " +e.getMessage()
							+" Causado por: "+e.getCause());
				}
				listTablas.setListData(nombreTablas);
			}
		});
		listTablas.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				btnExportarTabla.setEnabled(true);
				btnExportarTabla.setVisible(true);
			}
		});
	}
	protected File seleccionarFicheroXML() {
		// TODO Auto-generated method stub
		File f = null;
		JFileChooser jfc = new JFileChooser(".");
		FileNameExtensionFilter xmlFilter = new FileNameExtensionFilter("xml files (*.xml)", "xml");
		jfc.setFileFilter(xmlFilter);
		jfc.setDialogTitle("Seleccione un fichero (XML extension)");
		int seleccion = jfc.showOpenDialog(ventana);
		if (seleccion == JFileChooser.APPROVE_OPTION) {
			f=jfc.getSelectedFile();
			if (f.isDirectory()) {
				textField.setText(jfc.getSelectedFile().getAbsolutePath());
				btnImportar.setEnabled(true);
			}
			else {
				Utilidades.notificaError(ventana, "Error al seleccionar carpeta", null,
						f.getAbsolutePath()+" no es una carpeta");			
			}
		} 
		return f;
	}
}
