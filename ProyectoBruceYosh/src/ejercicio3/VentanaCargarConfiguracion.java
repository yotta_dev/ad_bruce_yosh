
package ejercicio3;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import util.Utilidades;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JPasswordField;

public class VentanaCargarConfiguracion extends JFrame {

	private JPanel contentPane;
	private JTextField textFieldIP;
	private JTextField textFieldUsu;
	static BaseDeDatos bd;
	private JPasswordField passwordField;
	public static JFrame frame= new JFrame();
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					frame = new VentanaCargarConfiguracion();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public VentanaCargarConfiguracion() {
		setResizable(false);
		setTitle("Cargar configuración");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblIp = new JLabel("IP");
		lblIp.setBounds(99, 60, 70, 15);
		contentPane.add(lblIp);
		
		JLabel lblUsuario = new JLabel("Usuario");
		lblUsuario.setBounds(99, 92, 70, 15);
		contentPane.add(lblUsuario);
		
		JLabel lblContrasea = new JLabel("Contrase\u00F1a");
		lblContrasea.setBounds(99, 130, 94, 15);
		contentPane.add(lblContrasea);
		
		textFieldIP = new JTextField();
		textFieldIP.setToolTipText("Example: localhost");
		textFieldIP.setBounds(208, 58, 114, 19);
		contentPane.add(textFieldIP);
		textFieldIP.setColumns(10);
		
		textFieldUsu = new JTextField();
		textFieldUsu.setToolTipText("Example: root");
		textFieldUsu.setBounds(208, 90, 114, 19);
		contentPane.add(textFieldUsu);
		textFieldUsu.setColumns(10);
		
		JButton btnCargar = new JButton("Conectar");
		btnCargar.setMnemonic('c');
		btnCargar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				bd = new BaseDeDatos();
				try {
					bd.conectar(textFieldIP.getText(), textFieldUsu.getText(), 
							String.valueOf(passwordField.getPassword()));
					ImportarExportarXMLGUI ventana = new ImportarExportarXMLGUI(frame);
					frame.setEnabled(false);
					frame.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
					ventana.setVisible(true);
					
				} catch (Exception e) {
					//VentanaCargarConfiguracion.this llama al jFrame a diferencia de poder this que llamaria
					//al actionListener del boton
					Utilidades.notificaError(frame, "Error al conectar a la BD", e, null);
				}
			}
		});
		btnCargar.setBounds(164, 177, 117, 25);
		contentPane.add(btnCargar);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(208, 128, 114, 19);
		contentPane.add(passwordField);
	}
}
