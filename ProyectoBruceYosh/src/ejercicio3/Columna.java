package ejercicio3;

public class Columna {
	String nombre;
	String tipo;
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public Columna(String nombre, String tipo) {
		super();
		this.nombre = nombre;
		this.tipo = tipo;
	}
	@Override
	public String toString() {
		return "Columna [nombre=" + nombre + ", tipo=" + tipo + "]";
	}
	
	
}
